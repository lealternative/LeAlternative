# LeAlternative

Come tutto quello che vedete su LeAlternative anche questo è un nuovo, importante, esperimento.

Questo tool di segnalazione vuole rendere più semplice la possibilità di segnalare errori, refusi, link sbagliati o anche inesattezze scritte all'interno del sito [LeAlternative.net](https://www.lealternative.net).

Potete utilizzarlo in autonomia creando un nuovo "problema" e specificando dove avete trovato l'errore oppure, ancora più semplicemente, troverete all'interno di **ogni** articolo un bottoncino a inizio articolo con scritto:

*🛠 Hai trovato errori o vuoi fare una segnalazione? Clicca QUI e usa il tool per le segnalazioni! 🛠*

Cliccandolo verrete immediatamente portati nella pagina dei problemi e, **automaticamente**, nel titolo del problema verrà inserito il titolo dell'articolo da cui siete partiti.

Vi basterà aggiungere quale è il problema riscontrato e verrà sistemato il prima possibile (o verrà spiegato perché non è possibile sistemarlo).

**Le segnalazioni sarebbe bene sempre farle utilizzando questo tool così da mantenere un po' di ordine e non rischiare che vengano perse tra tutti i vari social attivi. In questo modo è anche possibile mantenere una linea aperta con la redazione dove gli errori non vengono nascosti o sotterrati ma risolti insieme.**